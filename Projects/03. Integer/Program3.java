class Program3 {
	public static void main(String[] args) {
		int number = 4352175; // 4 + 3 + 5 + 2 + 1 + 7 + 5 = 27
		// 4352175 = 435217 * 10 + 5 
		int digit0 = number % 10; // digit0 = 5
		number = number / 10; // number = 435217
		int digit1 = number % 10; // digit1 = 7
		number = number / 10; // number = 43521
		int digit2 = number % 10; // digit2 = 1
		number = number / 10; // number = 4352
		int digit3 = number % 10; // digit3 = 2
		number = number / 10; // number = 435
		int digit4 = number % 10; // digit3 = 5
		number = number / 10; // number = 43
		int digit5 = number % 10; // digit3 = 3
		number = number / 10; // number = 4
		int digit6 = number % 10; // digit6 = 4;
		number = number / 10;

		int digitsSum = digit0 + digit1 + digit2 + digit3 
						+ digit4 + digit5 + digit6;

		System.out.println(digitsSum);
		System.out.println(number);    
	}
}