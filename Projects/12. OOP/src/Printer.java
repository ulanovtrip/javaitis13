/**
 * 10.02.2021
 * 12. OOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Printer {
    // fields - поля - состояние
    boolean isRed;
    String name;

    // конструктор с параметром, выполняет инициализацию объекта
    Printer(String name) {
        // кладет необходимое имя
        this.name = name;
        this.isRed = true;
    }

    Printer(String name, boolean isRed) {
        this.name = name;
        this.isRed = isRed;
    }

    // метод - поведение
    void printMessage(String message) {
        if (isRed) {
            System.err.println(name + " " + message);
        } else {
            System.out.println(name + " " + message);
        }
    }

    void printMessage(String message, String suffix) {
        if (isRed) {
            System.err.println(name + " " + message + " " + suffix);
        } else {
            System.out.println(name + " " + message + " " + suffix);
        }
    }
}
