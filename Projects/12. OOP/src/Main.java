import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
    	Scanner scanner = new Scanner(System.in);
	    Printer printer = new Printer("Best Printer");

	    printer.isRed = false;
	    printer.printMessage("Hello, Marsel!");
	    printer.printMessage("Bye, Marsel", "Chao");

	    Printer anotherPrinter = new Printer("New Printer", false);
	    anotherPrinter.printMessage("Hi!");
    }
}
