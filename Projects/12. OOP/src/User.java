/**
 * 10.02.2021
 * 12. OOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class User {
    String name;
    String password;
    int age;
    double height;

    User() {

    }

    User(String name) {
        this.name = name;
    }

    User(User other) {
        this.name = other.name;
        this.age = other.age;
        this.height = other.height;
        this.password = other.password;
    }
}
